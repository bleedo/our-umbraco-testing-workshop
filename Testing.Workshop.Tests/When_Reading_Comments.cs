﻿using System;
using Moq;
using NUnit.Framework;
using Testing.Workshop.Tests.Support;
using Testing.Workshop.Tests.Support.Domain;
using Testing.Workshop.Tests.Support.Umbraco;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.PublishedCache;

namespace Testing.Workshop.Tests
{
    public abstract class When_Reading_Comments
    {
        public const int ExpectedParentId = 5;

        protected UmbracoSupport UmbracoSupport;
        protected IReadingCommentsSupport ReadingCommentsSupport;

        [SetUp]
        public void SetupBase()
        {
            UmbracoSupport = new UmbracoSupport();
            UmbracoSupport.SetupUmbraco();
        }

        [TearDown]
        public void TearDownBase()
        {
            UmbracoSupport.TearDown();
        }

        [Test]
        public void Then_All_Comments_For_The_Current_Content_Is_Listed()
        {
            ReadingCommentsSupport.AddComment(parentId: 99);
            ReadingCommentsSupport.AddComment(parentId: ExpectedParentId);
            ReadingCommentsSupport.VerifyCommentsAreListed(parentId: ExpectedParentId, count: 1);
        }
    }

    [TestFixture]
    public class When_Reading_Comments_Domain : When_Reading_Comments
    {
        [SetUp]
        public void Setup()
        {
            ReadingCommentsSupport = new ReadingCommentsSupport();
        }
    }

    [TestFixture]
    public class When_Reading_Comments_Mvc : When_Reading_Comments
    {
        [SetUp]
        public void Setup()
        {
            var contentType = Mock.Of<IContentType>();

            Mock.Get(contentType).Setup(t => t.Id).Returns(1);

            Mock.Get(ApplicationContext.Current.Services.ContentTypeService)
                .Setup(s => s.GetContentType("Comment"))
                .Returns(contentType);

            Mock.Get(UmbracoSupport.CurrentPage)
                .Setup(page => page.Id)
                .Returns(ExpectedParentId);

            var contentCache = Mock.Of<IPublishedContentCache>();

            ReadingCommentsSupport = new Support.Mvc.ReadingCommentsSupport(UmbracoSupport);
        }
    }
}

namespace Testing.Workshop.Tests.Support
{
    public interface IReadingCommentsSupport
    {
        void VerifyCommentsAreListed(int parentId, int count);
        void AddComment(int parentId);
    }
}
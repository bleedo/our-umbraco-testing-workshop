namespace Testing.Workshop.Tests.Support
{
    public interface IPostingCommentsSupport
    {
        CommentResult PostValidComment();
        void WriteText(string text);
        void WriteEmail(string email);
        CommentResult PostComment();
        void VerifyAcceptedComment();
        void VerifyRejectedComment();
    }
}
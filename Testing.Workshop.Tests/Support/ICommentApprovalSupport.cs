namespace Testing.Workshop.Tests.Support
{
    public interface ICommentApprovalSupport
    {
        void AddPublishedComment();
        void AddUnapprovedComment();
        void ApproveLastUnapproved();
        void VerifyLastCommentIsVisible();
        void VerifyUnapprovedCommentsAreShown();
        void DenyLastUnapproved();
        void VerifyLastCommentIsDeleted();
    }
}
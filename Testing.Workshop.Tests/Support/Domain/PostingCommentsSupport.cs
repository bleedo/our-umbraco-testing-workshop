using Moq;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web.PublishedCache;

namespace Testing.Workshop.Tests.Support.Domain
{
    public class PostingCommentsSupport : IPostingCommentsSupport
    {
        private readonly IContentService contentService;
        private readonly CommentService commentService;
        private readonly IContent commentContent;

        public const int ExpectedParentId = 5;
        public const string ValidEmail = "victor@visitor.com";
        public const string ValidText = "Good article!";

        private Comment comment = new Comment();

        public PostingCommentsSupport()
        {
            contentService = Mock.Of<IContentService>();
            commentContent = Mock.Of<IContent>();
            commentService = new CommentService(contentService, Mock.Of<IPublishedContentCache>(), 1);
        }

        public CommentResult PostValidComment()
        {
            comment = ValidComment();
            return PostComment();
        }

        public void WriteText(string text)
        {
            comment.Text = text;
        }

        public void WriteEmail(string email)
        {
            comment.Email = email;
        }

        public CommentResult PostComment()
        {
            ContentServiceWillCreateContent(comment);
            return commentService.PostComment(comment);
        }

        private static Comment ValidComment()
        {
            return new Comment
            {
                Email = ValidEmail,
                Text = ValidText,
                ParentContentId = ExpectedParentId
            };
        }

        private void ContentServiceWillCreateContent(Comment comment)
        {
            Mock.Get(contentService)
                .Setup(s => s.CreateContent("Comment from " + comment.Email, comment.ParentContentId, "Comment", 0))
                .Returns(commentContent);
        }

        public void VerifyAcceptedComment()
        {
            Mock.Get(contentService).Verify(s => s.Save(commentContent, 0, true));
        }

        public void VerifyPropertyWasSet(string propertyAlias, string expectedValue)
        {
            Mock.Get(commentContent).Verify(c => c.SetValue(propertyAlias, expectedValue));
        }

        public void VerifyRejectedComment()
        {
            Mock.Get(contentService).Verify(s => s.Save(commentContent, 0, true), Times.Never);
        }

        public void SetParent(int parentId)
        {
            comment.ParentContentId = parentId;
        }
    }
}
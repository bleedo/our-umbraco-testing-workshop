﻿using System;
using System.Linq;
using NUnit.Framework;
using Umbraco.Tests.TestHelpers;

namespace Testing.Workshop.Tests.Support.Domain
{
    public class CommentApprovalSupport : CommentApprovalSupportBase, ICommentApprovalSupport
    {
        public void VerifyUnapprovedCommentsAreShown()
        {
            var commentsToApprove = GetCommentsToApprove();

            Assert.That(
                commentsToApprove,
                Is.EquivalentTo(Comments.Where(c => c.Published == false))
            );
        }

        public void DenyLastUnapproved()
        {
            var comment = Comments.Last(c => !c.Published);
            CommentsService.Deny(comment);
        }

        public void ApproveLastUnapproved()
        {
            var comment = Comments.Last(c => !c.Published);
            CommentsService.Approve(comment);
        }

        public CommentApprovalSupport()
        {
            SettingsForTests.ConfigureSettings(SettingsForTests.GenerateMockSettings());
        }
    }
}
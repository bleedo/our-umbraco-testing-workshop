using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Umbraco.Core.Models;

namespace Testing.Workshop.Tests.Support.Domain
{
    public class ReadingCommentsSupport : ReadingCommentsSupportBase, IReadingCommentsSupport
    {
        private IEnumerable<IPublishedContent> retrievedComments;

        public void VerifyCommentsAreListed(int parentId, int count)
        {
            retrievedComments = CommentService.GetComments(parentId);
            Assert.That(retrievedComments.ToList(), 
                Is.EquivalentTo(Comments[parentId]) &
                Has.Count.EqualTo(count)
            );
        }
    }
}
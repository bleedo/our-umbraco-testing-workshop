﻿using System.Web.Mvc;
using ApprovalTests;
using ApprovalTests.Reporters;
using Newtonsoft.Json;
using Testing.Workshop.Mvc;
using Testing.Workshop.Tests.Support.Umbraco;

namespace Testing.Workshop.Tests.Support.Mvc
{
    [UseReporter(typeof(DiffReporter))]
    public class ReadingCommentsSupport : ReadingCommentsSupportBase, IReadingCommentsSupport
    {
        private readonly CommentsController controller;

        public ReadingCommentsSupport(UmbracoSupport umbracoSupport)
        {
            controller = new CommentsController(umbracoSupport.UmbracoContext, ContentCache);
            var controllerContext = new ControllerContext(umbracoSupport.HttpContext, umbracoSupport.RouteData, controller);
            controller.ControllerContext = controllerContext;
        }

        public void VerifyCommentsAreListed(int parentId, int count)
        {
            var result = (ViewResult)controller.Comments();

            Approvals.VerifyJson(JsonConvert.SerializeObject(result.Model));
        }
    }
}

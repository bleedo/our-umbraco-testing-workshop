﻿using ApprovalTests;
using System;
using System.Linq;
using ApprovalTests.Namers;
using ApprovalTests.Reporters;
using Moq;
using Newtonsoft.Json;
using Testing.Workshop.Mvc;
using Testing.Workshop.Tests.Support.Umbraco;
using Umbraco.Core.Models;

namespace Testing.Workshop.Tests.Support.Mvc
{
    [UseReporter(typeof(DiffReporter))]
    public class CommentApprovalSupport : CommentApprovalSupportBase, ICommentApprovalSupport
    {
        private readonly UmbracoSupport umbracoSupport;
        private readonly CommentsApiController controller;

        public void VerifyUnapprovedCommentsAreShown()
        {
            var comments = controller.Unapproved();

            Approvals.VerifyJson(JsonConvert.SerializeObject(comments));
        }

        public void ApproveLastUnapproved()
        {
            controller.Approve(Comments.Last(c => !c.Published).Id);
        }

        public void DenyLastUnapproved()
        {
            controller.Deny(Comments.Last(c => !c.Published).Id);
        }

        public CommentApprovalSupport(UmbracoSupport umbracoSupport)
        {
            this.umbracoSupport = umbracoSupport;

            StubBlogPost(umbracoSupport);

            controller = new CommentsApiController(CommentsService);
        }

        private static void StubBlogPost(UmbracoSupport umbracoSupport)
        {
            var post = Mock.Of<IContent>();
            var postMock = Mock.Get(post);
            postMock.Setup(c => c.Id).Returns(1);
            postMock.Setup(c => c.Name).Returns("Tall trees have deep roots");
            Mock.Get(umbracoSupport.ServiceContext.ContentService)
                .Setup(s => s.GetById(It.IsAny<int>()))
                .Returns(post);
        }
    }
}

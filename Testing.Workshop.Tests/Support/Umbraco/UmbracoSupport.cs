﻿using System;
using System.Web;
using System.Web.Routing;
using Moq;
using Umbraco.Core;
using Umbraco.Core.Configuration.UmbracoSettings;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence;
using Umbraco.Core.Services;
using Umbraco.Tests.TestHelpers;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.Routing;

namespace Testing.Workshop.Tests.Support.Umbraco
{
    public class UmbracoSupport : BaseRoutingTest
    {
        private UmbracoContext umbracoContext;
        private ServiceContext serviceContext;
        private IUmbracoSettingsSection settings;
        private RoutingContext routingContext;
        private IPublishedContent currentPage;
        private RouteData routeData;

        public UmbracoContext UmbracoContext => umbracoContext;

        public new ServiceContext ServiceContext => serviceContext;

        public IPublishedContent CurrentPage => currentPage;

        public RouteData RouteData => routeData;

        public HttpContextBase HttpContext => GetHttpContextFactory("http://localhost", RouteData).HttpContext;

        public void SetupUmbraco()
        {
            InitializeFixture();
            Initialize();

            settings = SettingsForTests.GenerateMockSettings();
            SettingsForTests.ConfigureSettings(settings);
            umbracoContext = GetUmbracoContext("http://localhost", -1, new RouteData(), true);

            routeData = new RouteData();
            routingContext = GetRoutingContext("http://localhost", -1, routeData, true, settings);
            currentPage = Mock.Of<IPublishedContent>();
            var routeDefinition = new RouteDefinition
            {
                PublishedContentRequest =
                    new PublishedContentRequest(new Uri("http://localhost"), routingContext, settings.WebRouting,
                        s => new string[0])
                    {
                        PublishedContent = currentPage
                    }
            };
            routeData.DataTokens.Add("umbraco-route-def", routeDefinition);

            umbracoContext = routingContext.UmbracoContext;
        }

        public void DisposeUmbraco()
        {
            TearDown();
        }

        protected override ApplicationContext CreateApplicationContext()
        {
            serviceContext = MockHelper.GetMockedServiceContext();
            var appContext = new ApplicationContext(
                //assign the db context
                new DatabaseContext(Mock.Of<IDatabaseFactory>(), Logger, SqlSyntax, GetDbProviderName()),
                //assign the service context
                serviceContext,
                CacheHelper,
                ProfilingLogger);
            return appContext;
        }
    }
}

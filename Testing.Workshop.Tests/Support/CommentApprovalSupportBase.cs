using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web.PublishedCache;

namespace Testing.Workshop.Tests.Support
{
    public class CommentApprovalSupportBase
    {
        private IContentService contentService;
        protected List<IContent> Comments;
        protected CommentService CommentsService;

        public CommentApprovalSupportBase()
        {
            SetupContentService();
            SetupCommentService();
        }

        private const int ContentTypeId = 1;

        public void AddUnapprovedComment()
        {
            var unpublishedContent = Mock.Of<IContent>();
            StubProperties(Mock.Get(unpublishedContent));
            Comments.Add(unpublishedContent);
        }

        public void AddPublishedComment()
        {
            var publishedContent = Mock.Of<IContent>();
            var contentMock = Mock.Get(publishedContent);

            contentMock.Setup(c => c.Published).Returns(true);
            StubProperties(contentMock);

            Comments.Add(publishedContent);
        }

        public void VerifyLastCommentIsVisible()
        {
            Assert.That(Comments, Has.All.Property("Published").True);
        }

        public void VerifyLastCommentIsDeleted()
        {
            Mock.Get(contentService)
                .Verify(s => s.Delete(Comments.Last(), 0));
        }

        private static void StubProperties(Mock<IContent> commentMock)
        {
            commentMock.Setup(c => c.Id).Returns(1);
            commentMock.Setup(c => c.Properties)
                .Returns(new PropertyCollection(new List<Property>
                {
                    new Property(new PropertyType(Mock.Of<IDataTypeDefinition>(), "email"), "someone@somewhere.com"),
                    new Property(new PropertyType(Mock.Of<IDataTypeDefinition>(), "text"), "Here's a comment")
                }));
        }

        protected IEnumerable<IContent> GetCommentsToApprove()
        {
            var commentsToApprove = CommentsService.GetCommentsToApprove();
            return commentsToApprove;
        }

        private void SetupCommentService()
        {
            CommentsService = new CommentService(contentService, Mock.Of<IPublishedContentCache>(), ContentTypeId);
        }

        private void SetupContentService()
        {
            contentService = Mock.Of<IContentService>();
            Comments = new List<IContent>();

            Mock.Get(contentService)
                .Setup(c => c.GetContentOfContentType(ContentTypeId))
                .Returns(Comments);

            Mock.Get(contentService)
                .Setup(s => s.Publish(It.IsAny<IContent>(), 0))
                .Callback(
                    new Action<IContent, int>((c, user) => 
                        Mock.Get(c)
                            .Setup(c2 => c2.Published)
                            .Returns(true)
                    )
                );
        }
    }
}
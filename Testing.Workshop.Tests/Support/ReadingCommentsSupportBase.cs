using System.Collections.Generic;
using Moq;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.PublishedCache;

namespace Testing.Workshop.Tests.Support
{
    public class ReadingCommentsSupportBase
    {
        protected IPublishedContentCache ContentCache;
        protected Dictionary<int, IList<IPublishedContent>> Comments;
        protected CommentService CommentService;

        public ReadingCommentsSupportBase()
        {
            Comments = new Dictionary<int, IList<IPublishedContent>>();
            ContentCache = Mock.Of<IPublishedContentCache>();

            CommentService = new CommentService(Mock.Of<IContentService>(), ContentCache, 1);
        }

        public void AddComment(int parentId)
        {
            EnsureParent(parentId);

            var comment = Mock.Of<IPublishedContent>();
            var mock = Mock.Get(comment);

            mock.Setup(c => c.DocumentTypeAlias).Returns("Comment");

            var textMock = new Mock<IPublishedProperty>();
            textMock.Setup(p => p.Value).Returns("Intreaguing article!");
            mock.Setup(c => c.GetProperty("text", false)).Returns(textMock.Object);

            var emailMock = new Mock<IPublishedProperty>();
            emailMock.Setup(p => p.Value).Returns("tester@unit-testing.com");
            mock.Setup(c => c.GetProperty("email", false)).Returns(emailMock.Object);

            Comments[parentId].Add(comment);
        }

        private void EnsureParent(int parentId)
        {
            if (!Comments.ContainsKey(parentId))
            {
                var parent = Mock.Of<IPublishedContent>();

                var parentsComments = new List<IPublishedContent>();

                Mock.Get(ContentCache)
                    .Setup(c => c.GetById(It.IsAny<UmbracoContext>(), It.IsAny<bool>(), parentId))
                    .Returns(parent);

                Mock.Get(parent)
                    .Setup(c => c.Children)
                    .Returns(parentsComments);

                Comments.Add(parentId, parentsComments);
            }
        }
    }
}
﻿using NUnit.Framework;
using Testing.Workshop.Tests.Support.Domain;

namespace Testing.Workshop.Tests
{
    [TestFixture]
    public class When_Posting_Comments
    {
        private PostingCommentsSupport support;

        [SetUp]
        public void Setup()
        {
            support = new PostingCommentsSupport();
        }

        [Test]
        public void Then_Site_Accepts_Comment()
        {
            var result = support.PostValidComment();

            Assert.That(result.Success, Is.True);
        }

        [Test]
        public void Then_Comment_Is_Saved_Below_Content()
        {
            support.PostValidComment();

            support.VerifyAcceptedComment();
        }

        [Test]
        [TestCase("email", PostingCommentsSupport.ValidEmail)]
        [TestCase("text", PostingCommentsSupport.ValidText)]
        public void Then_Sets_Properties_On_Content(string propertyAlias, string expectedValue)
        {
            support.PostValidComment();

            support.VerifyPropertyWasSet(propertyAlias, expectedValue);
        }

        [Test]
        public void With_Invalid_Email_Address_Then_Is_Not_Saved()
        {
            support.WriteEmail("victor.visitor.com");
            support.WriteText(PostingCommentsSupport.ValidText);
            support.SetParent(PostingCommentsSupport.ExpectedParentId);

            support.PostComment();

            support.VerifyRejectedComment();
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase("\t")]
        [TestCase("    ")]
        public void With_Empty_Comment_Then_Is_Rejected(string text)
        {
            support.WriteEmail(PostingCommentsSupport.ValidEmail);
            support.WriteText(text);
            support.SetParent(PostingCommentsSupport.ExpectedParentId);

            support.PostComment();

            support.VerifyRejectedComment();
        }

        [Test]
        public void With_Invalid_Parent_Then_Is_Rejected()
        {
            support.WriteEmail(PostingCommentsSupport.ValidEmail);
            support.WriteText(PostingCommentsSupport.ValidText);
            support.SetParent(-99);

            support.PostComment();

            Assert.Inconclusive("Gotta figure out what happens in Umbraco. Root content when parent invalid?");
            support.VerifyRejectedComment();
        }

        [Test]
        public void Then_Provides_Validation_Messages()
        {
            var result = support.PostComment();

            Assert.That(
                result.Errors,
                Is.EquivalentTo(new[]
                {
                    "Invalid email address",
                    "Empty comment"
                })
            );
        }
    }
}

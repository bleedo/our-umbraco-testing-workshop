﻿using NUnit.Framework;
using Testing.Workshop.Mvc;

namespace Testing.Workshop.Tests
{
    [TestFixture]
    public class When_Finding_Avatars
    {
        [Test]
        [TestCase("mymailaddress@example.com ", "https://www.gravatar.com/avatar/3ed166612b487356a75e025eaa2ce8b1.jpg")]
        [TestCase("mymailaddress@Example.com", "https://www.gravatar.com/avatar/3ed166612b487356a75e025eaa2ce8b1.jpg")]
        [TestCase(" tester@unit-testing.com", "https://www.gravatar.com/avatar/fbc493336a41790984438064165f0769.jpg")]
        [TestCase(" lea@markedspartner.no", "https://www.gravatar.com/avatar/e01edb3cfd7c95926be8c85a0ea75bd6.jpg")]
        public void Then_Creates_Gravatar_Url(string email, string expectedUrl)
        {
            var gravatarUrl = new GravatarUrl(email);

            Assert.That(
                gravatarUrl,
                Has.Property("Url").EqualTo(expectedUrl)
                );
        }
    }
}

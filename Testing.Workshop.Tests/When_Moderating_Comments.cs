﻿using NUnit.Framework;
using Testing.Workshop.Tests.Support;
using Testing.Workshop.Tests.Support.Umbraco;

namespace Testing.Workshop.Tests
{
    public abstract class When_Moderating_Comments
    {
        protected ICommentApprovalSupport CommentApprovalSupport;

        [Test]
        public void Then_Unpublished_Comments_Are_Shown()
        {
            CommentApprovalSupport.AddPublishedComment();
            CommentApprovalSupport.AddUnapprovedComment();

            CommentApprovalSupport.VerifyUnapprovedCommentsAreShown();
        }

        [Test]
        public void Then_Comments_Can_Be_Approved()
        {
            CommentApprovalSupport.AddUnapprovedComment();
            CommentApprovalSupport.ApproveLastUnapproved();
            CommentApprovalSupport.VerifyLastCommentIsVisible();
        }

        [Test]
        public void Then_Comments_Can_Be_Denied()
        {
            CommentApprovalSupport.AddUnapprovedComment();
            CommentApprovalSupport.DenyLastUnapproved();
            CommentApprovalSupport.VerifyLastCommentIsDeleted();
        }
    }


    [TestFixture]
    public class When_Moderating_Comments_Domain : When_Moderating_Comments
    {
        [SetUp]
        public void Setup()
        {
            CommentApprovalSupport = new Support.Domain.CommentApprovalSupport();
        }
    }

    [TestFixture]
    public class When_Moderating_Comments_Mvc : When_Moderating_Comments
    {
        private UmbracoSupport umbracoSupport;

        [SetUp]
        public void Setup()
        {
            umbracoSupport = new UmbracoSupport();
            umbracoSupport.SetupUmbraco();
            CommentApprovalSupport = new Support.Mvc.CommentApprovalSupport(umbracoSupport);
        }

        [TearDown]
        public void TearDown()
        {
            umbracoSupport.DisposeUmbraco();
        }
    }

}

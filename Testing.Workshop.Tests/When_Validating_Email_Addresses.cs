﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using Testing.Workshop.Validation;

namespace Testing.Workshop.Tests
{
    [TestFixture]
    public class When_Validating_Email_Addresses
    {
        [Test]
        [TestCase(@"valid@email.com")]
        [TestCase(@"another.valid@email.address.com")]
        [TestCase(@"""() <>[]:,;@\\\""!#$%&'-/=?^_`{}| ~.a""@example.org", Ignore = "Let's not bother for now")]
        [TestCase(@"""very.(),:;<>[]\"".VERY.\""very@\\ \""very\"".unusual""@strange.example.com", Ignore = "Let's not bother for now")]
        public void Valid_Emails_Are_Valid(string validEmail)
        {
            var validator = new EmailValidator();
            var result = validator.Validate(validEmail);
            Assert.That(result, Is.True);
        }

        [Test]
        [TestCase(null)]
        [TestCase(@"Abc.example.com")]
        [TestCase(@"A@b@c@example.com")]
        [TestCase(@"just""not""right@example.com", Ignore = "Let's not bother for now")]
        [TestCase(@"this\ still\""not\\allowed@example.com", Ignore = "Let's not bother for now")]
        [TestCase(@"1234567890123456789012345678901234567890123456789012345678901234+x@example.com")]
        public void Invalid_Emails_Are_Invalid(string invalidEmail)
        {
            var validator = new EmailValidator();
            var result = validator.Validate(invalidEmail);
            Assert.That(result, Is.False);
        }
    }
}

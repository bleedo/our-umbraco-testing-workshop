﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Testing.Workshop.Mvc
{
    public class GravatarUrl
    {
        private readonly Encoding encoding = Encoding.UTF8;

        public GravatarUrl(string email)
        {
            var hash = MD5.Create().ComputeHash(encoding.GetBytes(email.Trim().ToLower()));
            var hashString = BitConverter.ToString(hash).Replace("-", "").ToLower();
            Url = $"https://www.gravatar.com/avatar/{hashString}.jpg";
        }

        public string Url { get; set; }

        public static string Create(string emailAddress)
        {
            return new GravatarUrl(emailAddress).Url;
        }
    }
}

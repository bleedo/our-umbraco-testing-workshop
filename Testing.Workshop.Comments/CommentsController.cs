﻿using System.Linq;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.PublishedCache;

namespace Testing.Workshop.Mvc
{
    public class CommentsController : SurfaceController
    {
        private readonly CommentService commentService;

        public CommentsController()
            : this(UmbracoContext.Current, UmbracoContext.Current.ContentCache.InnerCache)
        {
        }

        public CommentsController(UmbracoContext umbracoContext)
            : this(umbracoContext, umbracoContext.ContentCache.InnerCache)
        {
        }

        public CommentsController(UmbracoContext umbracoContext, IPublishedContentCache contentCache)
            : base(umbracoContext)
        {
            commentService = new CommentService(
                Services.ContentService,
                contentCache,
                Services.ContentTypeService.GetContentType("Comment").Id
            );
        }

        public ActionResult Comments()
        {
            var comments = commentService
                .GetComments(CurrentPage.Id)
                .Select(c => new CommentView
                {
                    Text = c.GetPropertyValue<string>("text"),
                    Avatar = GravatarUrl.Create(c.GetPropertyValue<string>("email"))
                })
                .ToArray();

            return View("Comments", comments);
        }

        public ActionResult PostComment(Comment comment)
        {
            comment.ParentContentId = CurrentPage.Id;
            commentService.PostComment(comment);
            return RedirectToCurrentUmbracoPage();
        }
    }
}

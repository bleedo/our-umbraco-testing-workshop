﻿namespace Testing.Workshop.Mvc
{
    public class CommentView
    {
        public string Text { get; set; }
        public string Avatar { get; set; }
    }
}

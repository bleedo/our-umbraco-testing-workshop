﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Controllers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Umbraco.Core.Models;
using Umbraco.Web.WebApi;

namespace Testing.Workshop.Mvc
{
    [CamelCasedApiController]
    public class CommentsApiController : UmbracoAuthorizedApiController
    {
        private readonly CommentService commentsService;
        private IEnumerable<IContent> unapprovedComments;

        public CommentsApiController()
        {
            commentsService = new CommentService(
                Services.ContentService,
                UmbracoContext.ContentCache.InnerCache,
                Services.ContentTypeService.GetContentType("Comment").Id
                );
            Initialize();
        }

        public CommentsApiController(CommentService commentsService)
        {
            this.commentsService = commentsService;
            Initialize();
        }

        [HttpGet]
        public IEnumerable<PostWithUnapprovedComments> Unapproved()
        {
            var unapprovedByPost = unapprovedComments.GroupBy(c => c.Parent());

            return unapprovedByPost.Select(pair =>
                new PostWithUnapprovedComments
                {
                    PostId = pair.Key.Id,
                    Name = pair.Key.Name,
                    Unapproved = pair.Select(comment =>
                        new CommentForApproval
                        {
                            Id = comment.Id,
                            Email = (string) comment.Properties["email"].Value,
                            Text = (string) comment.Properties["text"].Value
                        }
                    )
                }
            );
        }

        [HttpPost]
        public void Approve([FromBody]int id)
        {
            var comment = unapprovedComments.Single(c => c.Id == id);
            commentsService.Approve(comment);
        }

        [HttpPost]
        public void Deny([FromBody]int id)
        {
            var comment = unapprovedComments.Single(c => c.Id == id);
            commentsService.Deny(comment);
        }

        private void Initialize()
        {
            unapprovedComments = commentsService.GetCommentsToApprove();
        }
    }

    public class PostWithUnapprovedComments
    {
        public int PostId { get; set; }
        public string Name { get; set; }
        public IEnumerable<CommentForApproval> Unapproved { get; set; }
    }

    public class CommentForApproval
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Text { get; set; }
    }

    public class CamelCasedApiController : Attribute, IControllerConfiguration
    {
        public void Initialize(HttpControllerSettings controllerSettings, HttpControllerDescriptor controllerDescriptor)
        {
            controllerSettings.Formatters.Clear();
            controllerSettings.Formatters.Add(new JsonMediaTypeFormatter
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }
            });
        }
    }

}

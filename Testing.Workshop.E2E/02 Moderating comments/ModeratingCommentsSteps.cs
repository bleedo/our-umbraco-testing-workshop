﻿using System.Threading;
using TechTalk.SpecFlow;
using Testing.Workshop.Tests.Support;

namespace Testing.Workshop.E2E._02_Moderating_comments
{
    [Binding]
    public class ModeratingCommentsSteps
    {
        private readonly ICommentApprovalSupport support;

        public ModeratingCommentsSteps(ICommentApprovalSupport support)
        {
            this.support = support;
        }

        [Given(@"Victor has posted a comment")]
        public void GivenVictorHasPostedAComment()
        {
            support.AddUnapprovedComment();
        }

        [When(@"Mona approves Victor's comment")]
        public void WhenMonaApprovesVictorSComment()
        {
            support.ApproveLastUnapproved();


        }

        [Then(@"everyone can see the comment on the site")]
        public void ThenEveryoneCanSeeTheCommentOnTheSite()
        {
            support.VerifyLastCommentIsVisible();
        }

        [Given(@"Vera Venomous has posted a trolling comment")]
        public void GivenVeraVenomousHasPostedATrollingComment()
        {
            support.AddUnapprovedComment();
        }

        [When(@"Mona denies Vera's comment")]
        public void WhenMonaDeniesVeraSComment()
        {
            support.DenyLastUnapproved();
        }

        [Then(@"the comment is deleted")]
        public void ThenTheCommentIsDeleted()
        {
            support.VerifyLastCommentIsDeleted();
        }

    }
}

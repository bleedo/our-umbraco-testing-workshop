﻿@OnDashboard
Feature: 02 Moderating comments
	In order to avoid silly comments
	As a moderator
	I want to approve all comments on the site

    Personas
        - Mona Moderator
        - Victor Visitor
        - Vera Venomous

    Rules
        - Administrator must approve comments

Scenario: Approve comments
    Given Victor has posted a comment
	When Mona approves Victor's comment
    Then everyone can see the comment on the site

Scenario: Deny comments
    Given Vera Venomous has posted a trolling comment
	When Mona denies Vera's comment
    Then the comment is deleted

﻿using TechTalk.SpecFlow;
using Umbraco.TestHost;

namespace Testing.Workshop.E2E.Support.UI
{
    [Binding]
    public class UIBindings
    {
        #if UI_TEST

        private static DriverSupport driverSupport;

        [BeforeTestRun(Order=1)]
        public static void BeforeTestRun()
        {
            driverSupport = new DriverSupport();
            driverSupport.Initialize();
        }

        [AfterTestRun]
        public static void AfterTestRun()
        {
            driverSupport.Dispose();
        }

        [BeforeScenario(Order=1)]
        public void RegisterDriver()
        {
            ScenarioContext.Current.ScenarioContainer.RegisterInstanceAs(driverSupport);
        }

        [BeforeScenario("OnBlogPost")]
        public void SetupCommentingOnBlogPost()
        {
            var blogContext = ScenarioContext.Current.ScenarioContainer.Resolve<BlogContext>();
            driverSupport.Navigate(blogContext.Route);
        }

        [BeforeScenario("OnDashboard")]
        public void LogInToBackofficeCommentDashboard()
        {
            LoginToBackoffice(driverSupport);
        }

        public static void LoginToBackoffice(DriverSupport driverToUse)
        {
            driverToUse.Navigate("/umbraco/#");
            if (!driverToUse.IsLoggedIn())
            {
                driverToUse.LogIn();
            }
        }

#endif
    }
}

﻿using ApprovalTests;
using ApprovalTests.Reporters;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using Testing.Workshop.Tests.Support;
using Umbraco.TestHost;

namespace Testing.Workshop.E2E.Support.UI
{
    [UseReporter(typeof(DiffReporter))]
    public class ReadingCommentsSupport : IReadingCommentsSupport
    {
        private readonly ICommentApprovalSupport approvalSupport;
        private readonly DriverSupport driverSupport;
        private BlogContext blogContext;

        public ReadingCommentsSupport(ICommentApprovalSupport approvalSupport, DriverSupport driverSupport, BlogContext blogContext)
        {
            this.blogContext = blogContext;
            this.approvalSupport = approvalSupport;
            this.driverSupport = driverSupport;
        }

        public void VerifyCommentsAreListed(int parentId, int count)
        {
            ScenarioContext.Current.ScenarioContainer.Resolve<RefresherSupport>().Republish();

            driverSupport.Driver.Navigate().Refresh();
            var container = driverSupport.FindElement(By.ClassName("comments"));
            Approvals.VerifyHtml(container.GetAttribute("outerHTML"));
        }

        public void AddComment(int parentId)
        {
            approvalSupport.AddPublishedComment();
        }
    }
}

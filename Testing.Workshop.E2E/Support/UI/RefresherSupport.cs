﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;

namespace Testing.Workshop.E2E.Support.UI
{
    [Binding]
    public class RefresherSupport
    {
        private const string RefreshUrl = "http://localhost:9576/umbraco/dialogs/republish.aspx";
        private static DriverSupport refresherDriver;

#if UI_TEST

        [BeforeTestRun(Order=1)]
        public static void CreateDriverAndLogIn()
        {
            refresherDriver = new DriverSupport();
            refresherDriver.Initialize();
            refresherDriver.Driver.Manage().Window.Position = new Point(1000, 500);
            refresherDriver.Driver.Manage().Window.Size = new Size(400, 300);
        }

        [AfterTestRun]
        public static void KillDriver()
        {
            refresherDriver.Dispose();
        }
#endif

        [BeforeScenario("Republish")]
        public void Republish()
        {
#if UI_TEST
           if (refresherDriver.Driver.Url != RefreshUrl)
            {
                UIBindings.LoginToBackoffice(refresherDriver);
                refresherDriver.Wait.Until(d => d.FindElement(By.ClassName("umb-avatar")));
                refresherDriver.Driver.Url = RefreshUrl;
            }
            else
            {
                refresherDriver.Driver.Url = RefreshUrl;
                refresherDriver.Driver.Navigate().Refresh();
            }
            refresherDriver.Wait.Until(d => d.FindElement(By.Id("body_bt_go"))).Click();
        }
#endif
        }

    }
}
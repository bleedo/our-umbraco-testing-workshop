﻿using System;
using System.IO;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using Umbraco.Core;
using Umbraco.TestHost;
using Umbraco.Web;

namespace Testing.Workshop.E2E.Support.UI
{
    [Binding]
    public class UmbracoBindings
    {
        private static UmbracoSupport umbracoSupport;
        private static BlogContextController blogController;
        private static BlogContext blogContext;
        private static bool republished = false;

#if UI_TEST

        [BeforeTestRun(Order = 2)]
        public static void StartUmbraco()
        {
            var umbracoPath = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\Testing.Workshop.Web"));
            umbracoSupport = new UmbracoSupport(umbracoPath);
            umbracoSupport.StartUmbraco();

            blogController = umbracoSupport.Create<BlogContextController>();
            blogContext = blogController.CreateBlogPost();

            var refresher = new RefresherSupport();
            refresher.Republish();
        }

        [AfterTestRun]
        public static void StopUmbraco()
        {
            blogController.Cleanup();
            umbracoSupport.StopUmbraco();
        }

        [BeforeScenario(Order=1)]
        public void RegisterUmbracoSupport()
        {
            ScenarioContext.Current.ScenarioContainer.RegisterInstanceAs(umbracoSupport);
        }

        [BeforeScenario(Order=1)]
        public void RegisterBlogContext()
        {
            Console.WriteLine($"Blog ID: {blogContext.Id}");
            ScenarioContext.Current.ScenarioContainer.RegisterInstanceAs(blogContext);
        }

        [BeforeScenario]
        public void RemoveComments()
        {
            blogController.RemoveChildren();
        }
#endif

    }

}

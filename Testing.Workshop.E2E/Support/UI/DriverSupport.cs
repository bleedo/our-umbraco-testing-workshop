﻿using System;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using static System.String;

namespace Testing.Workshop.E2E.Support.UI
{
    public class DriverSupport
    {
        private readonly string baseUrl = "http://localhost:9576";
        private RemoteWebDriver driver;

        public RemoteWebDriver Driver => driver;

        private WebDriverWait wait;
        public WebDriverWait Wait => wait;

        public void Initialize()
        {
            driver = new ChromeDriver();
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
        }

        public void Dispose()
        {
            driver.Dispose();
        }

        public void Navigate(string relativeUrl)
        {
            var fullUrl = Concat(baseUrl, relativeUrl);
            if (driver.Url != fullUrl)
                driver.Url = fullUrl;
            else
                driver.Navigate().Refresh();
        }

        public bool IsLoggedIn()
        {
            var avatarElements = driver.FindElements(By.ClassName("umb-avatar"));
            return avatarElements.Any();
        }

        public void LogIn()
        {
            var userName = wait.Until(d => d.FindElement(By.Name("username")));
            userName.SendKeys("admin@admin.com");
            driver.FindElement(By.Name("password")).SendKeys("adminadmin");
            driver.FindElement(By.CssSelector("button[type='submit']")).Click();
        }

        public void Click(string linkText)
        {
            driver.FindElement(By.LinkText(linkText)).Click();
        }

        public RemoteWebElement FindElement(By @by)
        {
            return (RemoteWebElement)driver.FindElement(@by);
        }
    }
}

﻿using System;
using System.Linq;
using System.Threading;
using ApprovalTests;
using ApprovalTests.Reporters;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using Testing.Workshop.Tests.Support;
using Umbraco.TestHost;

namespace Testing.Workshop.E2E.Support.UI
{
    [UseReporter(typeof(DiffReporter))]
    public class CommentApprovalSupport : ICommentApprovalSupport
    {
        private readonly DriverSupport driverSupport;
        private readonly UmbracoSupport umbracoSupport;
        private readonly BlogContext blogContext;
        private readonly RefresherSupport refresherSupport;
        private readonly RemoteWebDriver driver;

        private int lastCommentId;

        public CommentApprovalSupport(DriverSupport driverSupport, UmbracoSupport umbracoSupport, BlogContext blogContext, RefresherSupport refresherSupport)
        {
            this.driverSupport = driverSupport;
            this.umbracoSupport = umbracoSupport;
            this.blogContext = blogContext;
            this.refresherSupport = refresherSupport;
            this.driver = driverSupport.Driver;
        }

        public void AddUnapprovedComment()
        {
            var controller = umbracoSupport.Create<CommentController>();
            var result = controller.CreateComment(blogContext.Id, "A comment", "from@someone.com");
            lastCommentId = result.Id;
        }

        public void VerifyUnapprovedCommentsAreShown()
        {
            throw new NotImplementedException();
        }

        public void ApproveLastUnapproved()
        {
            NavigateToApproval();
            var lastComment = FindLastComment();

            var approveButton = lastComment.FindElement(By.CssSelector(".approve"));
            approveButton.Click();

            refresherSupport.Republish();
        }

        public void DenyLastUnapproved()
        {
            NavigateToApproval();
            var lastComment = FindLastComment();

            var denyButton = lastComment.FindElement(By.CssSelector(".deny"));
            denyButton.Click();

            driverSupport.Wait.Until(d => !d.FindElements(By.ClassName(".comment")).Any());

            refresherSupport.Republish();
        }

        public void VerifyLastCommentIsVisible()
        {
            driverSupport.Navigate(blogContext.Route);
            var comments = driver.FindElement(By.ClassName("comments"));
            Approvals.VerifyHtml(comments.GetAttribute("outerHTML"));
        }

        public void VerifyLastCommentIsDeleted()
        {
            var verifyer = umbracoSupport.Create<VerifyIdDoesNotExist>();
            verifyer.Verify(lastCommentId);
        }

        public void AddPublishedComment()
        {
            var controller = umbracoSupport.Create<CommentController>();
            var result = controller.CreateComment(blogContext.Id, "A comment", "from@someone.com");
            controller.Publish(result.Id);
            lastCommentId = result.Id;
        }

        private void NavigateToApproval()
        {
            driverSupport.Navigate("/umbraco/#");
            driverSupport.Click("Comment approval");
        }

        private IWebElement FindLastComment()
        {
            var postLabel = driver.FindElement(By.XPath($"//*[contains(text(), '{blogContext.Name}')]"));
            var postRow = postLabel.FindElement(By.XPath("./.."));
            postLabel.Click();
            var lastComment = postRow.FindElement(By.CssSelector(".comment:last-child"));
            return lastComment;
        }
    }
}

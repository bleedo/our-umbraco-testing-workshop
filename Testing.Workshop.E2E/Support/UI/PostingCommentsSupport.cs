﻿using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium.Remote;
using Testing.Workshop.Tests.Support;
using Umbraco.TestHost;

namespace Testing.Workshop.E2E.Support.UI
{
    public class PostingCommentsSupport : IPostingCommentsSupport
    {
        private readonly DriverSupport driverSupport;
        private readonly UmbracoSupport umbracoSupport;
        private readonly BlogContext blogContext;
        private readonly RemoteWebDriver driver;

        public PostingCommentsSupport(DriverSupport driverSupport, UmbracoSupport umbracoSupport, BlogContext blogContext)
        {
            this.driverSupport = driverSupport;
            this.umbracoSupport = umbracoSupport;
            this.blogContext = blogContext;
            this.driver = driverSupport.Driver;
        }

        public CommentResult PostValidComment()
        {
            WriteText("A valid comment");
            WriteText("a.valid@email.address");
            PostComment();

            return null;
        }

        public void WriteText(string text)
        {
            var emailBox = driver.FindElementByCssSelector("[name='Text']");
            emailBox.SendKeys(text);
        }

        public void WriteEmail(string email)
        {
            var emailBox = driver.FindElementByCssSelector("[name='Email']");
            emailBox.SendKeys(email);
        }

        public CommentResult PostComment()
        {
            var submitButton = driver.FindElementByCssSelector("button[type='submit']");
            submitButton.Click();

            return null;
            // TODO: Investigate whether we can remove return value from this
        }

        public void VerifyAcceptedComment()
        {
            var children = GetPostChildren();
            Assert.That(children, Has.Count.EqualTo(1));
        }

        public void VerifyRejectedComment()
        {
            var children = GetPostChildren();
            Assert.That(children, Has.Count.Zero);
        }

        private List<Dictionary<string, string>> GetPostChildren()
        {
            var childPropertyEnumerator = umbracoSupport.Create<ChildPropertyEnumerator>();
            var children = childPropertyEnumerator.GetChildren(blogContext.Id);
            return children;
        }
    }
}

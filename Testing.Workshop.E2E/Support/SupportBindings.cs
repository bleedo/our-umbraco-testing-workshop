﻿using TechTalk.SpecFlow;
using Testing.Workshop.Tests.Support;

namespace Testing.Workshop.E2E.Support
{
    [Binding]
    public class SupportBindings
    {
        [BeforeScenario]
        public void SetupSupportClasses()
        {
#if UI_TEST
            ScenarioContext.Current.ScenarioContainer.RegisterTypeAs<UI.ReadingCommentsSupport, IReadingCommentsSupport>();
            ScenarioContext.Current.ScenarioContainer.RegisterTypeAs<UI.PostingCommentsSupport, IPostingCommentsSupport>();
            ScenarioContext.Current.ScenarioContainer.RegisterTypeAs<UI.CommentApprovalSupport, ICommentApprovalSupport>();
#else
            ScenarioContext.Current.ScenarioContainer.RegisterTypeAs<Tests.Support.Domain.ReadingCommentsSupport, IReadingCommentsSupport>();
            ScenarioContext.Current.ScenarioContainer.RegisterTypeAs<Tests.Support.Domain.PostingCommentsSupport, IPostingCommentsSupport>();
            ScenarioContext.Current.ScenarioContainer.RegisterTypeAs<Tests.Support.Domain.CommentApprovalSupport, ICommentApprovalSupport>();
#endif
        }
    }
}

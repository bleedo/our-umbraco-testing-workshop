﻿@OnBlogPost
Feature: 03 Reading comments
	In order to see other peoples opinions
	As a reader
	I want to see other's comments on blog posts

Scenario: Read comments
	Given a blog post with comments
	When I visit it
	Then I should see the comments

﻿using System.Threading;
using TechTalk.SpecFlow;
using Testing.Workshop.Tests;
using Testing.Workshop.Tests.Support;
using Testing.Workshop.Tests.Support.Domain;
using Testing.Workshop.Tests.Support.Umbraco;

namespace Testing.Workshop.E2E._03_Reading_comments
{
    [Binding]
    class ReadingCommentsSteps
    {
        private readonly UmbracoSupport umbracoSupport;
        private readonly IReadingCommentsSupport readingCommentsSupport;

        public ReadingCommentsSteps(UmbracoSupport umbracoSupport, IReadingCommentsSupport readingCommentsSupport)
        {
            this.umbracoSupport = umbracoSupport;
            this.readingCommentsSupport = readingCommentsSupport;
        }

        [BeforeScenario]
        public void BeforeScenario()
        {
            umbracoSupport.SetupUmbraco();
        }

        [AfterScenario]
        public void AfterScenario()
        {
            umbracoSupport.DisposeUmbraco();
        }

        [Given(@"a blog post with comments")]
        public void GivenABlogPostWithComments()
        {
            readingCommentsSupport.AddComment(1);
            readingCommentsSupport.AddComment(1);
            Thread.Sleep(100);
        }

        [When(@"I visit it")]
        public void WhenIVisitIt()
        {
            // TODO: Find support for visiting blog post.
            // This is really incidental details, isn't it?
            //readingCommentsSupport.VisitBlogPost(1);

        }

        [Then(@"I should see the comments")]
        public void ThenIShouldSeeTheComments()
        {
            readingCommentsSupport.VerifyCommentsAreListed(1, 2);
        }

        [Then(@"an avatar of the poster by each comment")]
        public void ThenAnAvatarOfThePosterByEachComment()
        {
        }


    }
}

﻿using Moq;
using NUnit.Framework;
using TechTalk.SpecFlow;
using Testing.Workshop.Tests;
using Testing.Workshop.Tests.Support;
using Testing.Workshop.Tests.Support.Domain;
using Umbraco.Core.Services;

namespace Testing.Workshop.E2E._01_Posting_comments
{
    [Binding]
    public class PostingCommentsSteps
    {
        private readonly IPostingCommentsSupport support;

        public PostingCommentsSteps(IPostingCommentsSupport support)
        {
            this.support = support;
        }

        [Given(@"(?:.+|he) writes a comment")]
        public void GivenHeWritesAComment()
        {
            support.WriteText("A comment from the user");
        }

        [Given(@"(?:.+|he) doesn't write a comment")]
        public void GivenHeDoesntWriteAComment()
        {
            support.WriteText("    ");
        }

        [Given(@"provides a valid email address")]
        public void GivenProvidesAValidE_MailAddress()
        {
            support.WriteEmail("a.valid@email.address.com");
        }

        [Given(@"provides an invalid email address")]
        public void GivenProvidesAnInvalidE_MailAddress()
        {
            support.WriteEmail("an.invalid-email.address.com");
        }

        [When(@"he submits the comment")]
        public void WhenHeSubmitsTheComment()
        {
            support.PostComment();
        }

        [Then("the site accepts the comment")]
        public void ThenTheSiteAcceptsTheComment()
        {
            support.VerifyAcceptedComment();
        }

        [Then("the site rejects the comment")]
        public void ThenTheSiteRejectsTheComment()
        {
            support.VerifyRejectedComment();
        }

    }
}

﻿@OnBlogPost
Feature: 01 Posting comments
    In order to share my thoughts with the author and other readers
    As a reader
    I want to post a comment

    Personas
        - Victor Visitor

    Rules
        - Visitor must provide a valid e-mail address
        - Must provide a non-whitespace comment

Scenario: Post a comment
    Given Victor writes a comment
    And provides a valid email address
    When he submits the comment
    Then the site accepts the comment

Scenario: Submit an empty comment
    Given Victor doesn't write a comment
    And provides a valid email address
    When he submits the comment
    Then the site rejects the comment

Scenario: Provide invalid e-mail address
    Given Victor writes a comment
    And provides an invalid email address
    When he submits the comment
    Then the site rejects the comment

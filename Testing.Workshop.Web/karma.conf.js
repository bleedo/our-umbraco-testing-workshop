﻿module.exports = function(config) {
    config.set({
        files: [
            "umbraco/lib/angular/1.1.5/angular.js",
            "umbraco/lib/angular/1.1.5/angular-mocks.js",
            "umbraco/lib/jquery/jquery.min.js",
            "App_Plugins/Comments/**/*.js",
            "App_Plugins/Comments/**/*.html"
        ],
        preprocessors: {
            "App_Plugins/Comments/**/*.html": ["ng-html2js"]
        },
        frameworks: [
            "jasmine"
        ],
        reporters: [
            "progress",
            "kjhtml"
        ],
        browsers: [
            "Chrome"
        ],
        client: {
            clearContext: false
        },
        ngHtml2JsPreprocessor: {
            prependPrefix: "/",
            moduleName: "comments"
        }
    });
}
﻿(function () {

    var module = angular.module("comments", []),
        directive = {
            restrict: "E",
            templateUrl: "/App_Plugins/Comments/comments.dashboard.directive.html",
            replace: true,
            controller: "comments.dashboard.controller"
        };

    function commentsDashboardController(scope, http) {
        var expanded = [];

        function load() {
            http.get("/umbraco/backoffice/api/commentsapi/unapproved")
                .then(function (response) {
                    scope.posts = response.data;
                });
        }

        scope.expanded = function(post) {
            return expanded.indexOf(post) > -1;
        }

        scope.toggle = function(post) {
            var index = expanded.indexOf(post);
            if (index > -1) {
                expanded.splice(index, 1);
            } else {
                expanded.push(post);
            }
        }

        scope.extract = function(value) {
            var lines = value.split('\n');
            if (lines.length > 2) {
                return lines[0] + "\n" + lines[1] + "\n...";
            } else {
                return value;
            }
        };

        scope.approve = function (post, comment) {
            http.post("/umbraco/backoffice/api/commentsapi/approve", comment.id)
                .then(function() {
                    var index = post.unapproved.indexOf(comment);
                    post.unapproved.splice(index, 1);
                });

        }

        scope.deny = function (post, comment) {
            http.post("/umbraco/backoffice/api/commentsapi/deny", comment.id)
                .then(function () {
                    var index = post.unapproved.indexOf(comment);
                    post.unapproved.splice(index, 1);
                });
        }

        scope.posts = [];

        load();
    }

    module.controller("comments.dashboard.controller", [
        "$scope",
        "$http",
        commentsDashboardController
    ]);

    module.directive("commentsDashboard", function () { return directive; });

    try {
        angular.module("umbraco").requires.push("comments");
    } catch (e) {
    }

}());

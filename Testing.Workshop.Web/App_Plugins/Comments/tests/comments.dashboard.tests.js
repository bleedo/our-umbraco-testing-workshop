﻿/* globals: describe, beforeEach, afterEach, it, expect, module, inject */

(function () {
    var sampleData; // see below

    describe("comments dashboard", function() {
        var compile,
            scope,
            httpBackend,
            element;

        beforeEach(module("comments"));
        beforeEach(setupUiPreview);

        beforeEach(inject(function ($compile, $rootScope, $httpBackend) {
            compile = $compile;
            scope = $rootScope;
            httpBackend = $httpBackend;

            element = compile("<comments-dashboard/>")(scope);
            $("#sample-ui").append(element);

            createSampleData();
            httpBackend
                .whenGET("/umbraco/backoffice/api/commentsapi/unapproved")
                .respond(sampleData);

            scope.$digest();
            httpBackend.flush();
        }));

        it("shows unapproved comments by post", function () {
            expect(element.text()).toMatch(ignoreSpace(
                'Tall trees have deep roots                 2 ' +
                'Only the curious have something to find    1 '
            ));
        });

        it("shows posts when expanded", function () {
            $(".post:first", element).click();

            expect(element.text()).toMatch(ignoreSpace(
                'Tall trees have deep roots                 2 ' +
                '    From someone@somewhere.com' +
                '    This is a really nice post.' +
                '    I guess we should show the first two lines here' +
                '    ...' +
                '    Approve Deny' +
                '    From someone@somewhereelse.com' +
                '    This however, should show in full ' +
                '    Approve Deny ' +
                'Only the curious have something to find    1 $'
            ));
        });

        it("hides posts when collapsed", function () {
            $(".post:first", element).click();
            $(".post:first", element).click();

            expect(element.text()).toMatch(ignoreSpace(
                'Tall trees have deep roots                 2 ' +
                'Only the curious have something to find    1 '
            ));
        });

        it("sends approval to server when post is approved", function() {
            httpBackend
                .expectPOST("/umbraco/backoffice/api/commentsapi/approve", 3)
                .respond(200);

            $(".post:first", element).click();
            $(".comment:first .approve", element).click();

            httpBackend.verifyNoOutstandingExpectation();
        });

        it("removes post when approved", function() {
            httpBackend
                .expectPOST("/umbraco/backoffice/api/commentsapi/approve", 3)
                .respond(200);

            $(".post:first", element).click();
            $(".comment:first .approve", element).click();

            httpBackend.flush();

            expect($(".comment").length).toBe(1);
        });

        it("sends denial to server when post is denied", function() {
            httpBackend
                .expectPOST("/umbraco/backoffice/api/commentsapi/deny", 3)
                .respond(200);

            $(".post:first", element).click();
            $(".comment:first .deny", element).click();

            httpBackend.verifyNoOutstandingExpectation();
        });

        it("removes post when denied", function () {
            httpBackend
                .expectPOST("/umbraco/backoffice/api/commentsapi/deny", 3)
                .respond(200);

            $(".post:first", element).click();
            $(".comment:first .deny", element).click();

            httpBackend.flush();

            expect($(".comment").length).toBe(1);
        });
    });

    function setupUiPreview() {
        $("#sample-ui").remove();
        $("<div id=\"sample-ui\"></div>").appendTo("body");
    }

    function ignoreSpace(value) {
        return value.replace(/\s+/g, "\\s*");
    }

    function createSampleData() {

        sampleData = [
            {
                name: "Tall trees have deep roots",
                postId: 1,
                unapproved: [
                    {
                        id: 3,
                        text: "This is a really nice post.\nI guess we should show the first two lines here\nAnd skip this one.",
                        email: "someone@somewhere.com"
                    },
                    {
                        id: 4,
                        text: "This however, should show in full",
                        email: "someone@somewhereelse.com"
                    }
                ]
            },
            {
                name: "Only the curious have something to find",
                postId: 2,
                unapproved: [
                    {
                        id: 5,
                        text: "Now this is just plain rude.\nLet's not approve this one.",
                        email: "someone@somewhere.com"
                    }
                ]
            }
        ];

    }

}());
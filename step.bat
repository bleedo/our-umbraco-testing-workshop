@echo off

if "%1" == "" (
echo  1 Starting fresh, posting comments
echo  2 First scenario failing
echo  3 Cheating passed first scenario
echo  4 Testing invalid emails
echo  5 Email tests pass
echo  6 Moderating comments
echo  7 Unit tests for posting comments

echo  9 Posting comments pass

echo 99 Fin!
)

if "%1" == "1" git checkout 02510017271e4b170fc1d017039812c15571f715 & echo Starting fresh, posting comments
if "%1" == "2" git checkout 2728bd51f170127a9e070ebf604a1788f219b49c & echo First scenario failing
if "%1" == "3" git checkout 7f78f60642725349ba9a8a3f32f92951a8a17971 & echo Cheating passed first scenario
if "%1" == "4" git checkout d3c8b0c1389853ccb2a0bdffea2606364e6283d1 & echo Testing invalid emails
if "%1" == "5" git checkout 63bc4b73bd749cdae28eb79cb276dfd88f61cc13 & echo Email tests pass
if "%1" == "6" git checkout 8a1712ef0af307d3682197c011ea7cb253ddd809 & echo Moderating comments
if "%1" == "7" git checkout 6c22194f66f1fd21c8601dc2bd45bbdba84afaae & echo Unit tests for posting comments
rem find commit before command refactoring
if "%1" == "9" git checkout 006952552fea684c9621ab7d7f2d9206b0d4e04f & echo Posting comments pass

if "%1" == "99" git checkout 0c672d30af6bdc75cabf7b66cecee22399437a4c & echo Fin!
﻿using System;
using System.IO;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Web;
using Umbraco.Core;

namespace Umbraco.TestHost
{
    public class UmbracoSupport
    {
        private readonly string umbracoPath;
        private AppDomain umbracoDomain;
        private UmbracoController controller;

        public UmbracoSupport(string umbracoPath)
        {
            this.umbracoPath = umbracoPath;
        }

        public void StartUmbraco()
        {
            var path = umbracoPath;

            umbracoDomain = AppDomain.CreateDomain(
                "Umbraco",
                new Evidence(),
                new AppDomainSetup
                {
                    ApplicationBase = path,
                    PrivateBinPath = Path.Combine(path, "bin"),
                    ConfigurationFile = Path.Combine(path, "web.config"),

                }
            );

            // Of course there's a call to HttpRuntime.AppDomainAppId inthe database cache messenger, 
            // so we have to do some deep domain data overrides. 
            // However, this actually makes most of the stuff in System.Web.HttpRuntime work. :)
            umbracoDomain.SetData(".appDomain", "UmbracoTestHost");
            umbracoDomain.SetData(".appId", "UmbracoTestHost");
            umbracoDomain.SetData(".appPath", path);
            umbracoDomain.SetData(".appVPath", "/");

            controller = (UmbracoController)umbracoDomain.CreateInstanceAndUnwrap(GetType().Assembly.FullName, typeof(UmbracoController).FullName);
            controller.RunUmbraco();
        }

        public void StopUmbraco()
        {
            controller.DisposeApplication();
        }

        public T Create<T>()
        {
            return (T)umbracoDomain.CreateInstanceAndUnwrap(GetType().Assembly.FullName, typeof(T).FullName);
        }
    }

    public class UmbracoController : MarshalByRefObject
    {
        private static bool outputReady = false;
        private UmbracoApplicationUnderTest application;
        private static TestHostOutput outputWindow;

        public void RunUmbraco()
        {
            //outputWindow = new TestHostOutput();
            //outputWindow.Show();
            //var writer = new TestHostWriter(outputWindow);
            //Console.SetOut(writer);

            Console.WriteLine("Starting Umbraco");

            application = new UmbracoApplicationUnderTest();
            application.Start(null, EventArgs.Empty);
        }

        public void DisposeApplication()
        {
            application.Dispose();

            Console.WriteLine("Stopping Umbraco");
            //outputWindow.Kill();
        }
    }

    public class TestHostWriter : TextWriter
    {
        private readonly TestHostOutput outputWindow;

        public TestHostWriter(TestHostOutput outputWindow)
        {
            this.outputWindow = outputWindow;
        }

        public override Encoding Encoding { get { return Encoding.UTF8; } }

        public override void Write(char value)
        {
            outputWindow.AddChar(value);
        }
    }
}

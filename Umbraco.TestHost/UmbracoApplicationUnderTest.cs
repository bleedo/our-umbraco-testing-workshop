﻿using System;
using System.IO;
using System.Linq;
using Examine.Config;
using umbraco.editorControls;
using umbraco.interfaces;
using Umbraco.Core;
using Umbraco.Core.Sync;
using Umbraco.Web;

namespace Umbraco.TestHost
{
    public sealed class UmbracoApplicationUnderTest : UmbracoApplicationBase
    {
        public string BaseDirectory { get; private set; }
        public string DataDirectory { get; private set; }

        protected override IBootManager GetBootManager()
        {
            var binDirectory = new DirectoryInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin"));
            BaseDirectory = ResolveBasePath(binDirectory);
            DataDirectory = Path.Combine(BaseDirectory, "app_data");

            AppDomain.CurrentDomain.SetData("DataDirectory", DataDirectory);

            return new BootManager(this, BaseDirectory);
        }

        public void Start(object sender, EventArgs e)
        {
            base.Application_Start(sender, e);
        }

        private string ResolveBasePath(DirectoryInfo currentFolder)
        {
            var folders = currentFolder.GetDirectories();
            if (folders.Any(x => x.Name.Equals("app_data", StringComparison.OrdinalIgnoreCase)) &&
                folders.Any(x => x.Name.Equals("config", StringComparison.OrdinalIgnoreCase)))
            {
                return currentFolder.FullName;
            }

            if (currentFolder.Parent == null)
                throw new Exception("Base directory containing an 'App_Data' and 'Config' folder was not found." +
                    " These folders are required to run this console application as it relies on the normal umbraco configuration files.");

            return ResolveBasePath(currentFolder.Parent);
        }
    }


    public class BootManager : CoreBootManager
    {
        public BootManager(UmbracoApplicationBase umbracoApplication, string baseDirectory)
            : base(umbracoApplication)

        {
            //This is only here to ensure references to the assemblies needed for the DataTypesResolver
            //otherwise they won't be loaded into the AppDomain.
            var interfacesAssemblyName = typeof(IDataType).Assembly.FullName;
            var editorControlsAssemblyName = typeof(uploadField).Assembly.FullName;

            base.InitializeApplicationRootPath(baseDirectory);
        }

        /// <summary>
        /// Can be used to initialize our own Application Events
        /// </summary>
        protected override void InitializeApplicationEventsResolver()
        {
            base.InitializeApplicationEventsResolver();
        }

        /// <summary>
        /// Can be used to add custom resolvers or overwrite existing resolvers once they are made public
        /// </summary>
        protected override void InitializeResolvers()
        {
            base.InitializeResolvers();

            // Setup test host to publish refresh messages via the database
            ServerMessengerResolver.Current.SetServerMessenger(new BatchedDatabaseServerMessenger(
                ApplicationContext,
                true,
                new DatabaseServerMessengerOptions
                {
                    InitializingCallbacks = new Action[0]
                }));
        }

    }

}

﻿using System;
using Testing.Workshop;
using Umbraco.Core;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace Umbraco.TestHost
{
    public class CommentController : MarshalByRefObject
    {
        private readonly IContentService contentService;

        public CommentController()
        {
            contentService = ApplicationContext.Current.Services.ContentService;
        }

        public CommentResult CreateComment(int blogPostId, string text, string email)
        {
            var createCommentCommand = new CreateCommentCommand(
                contentService,
                new Comment
                {
                    ParentContentId = blogPostId,
                    Text = text,
                    Email = email
                }
            );

            var commentResult = createCommentCommand.CreateComment();
            Console.WriteLine("Created comment with id {0}", commentResult.Id);

            return commentResult;
        }

        public void Publish(int id)
        {
            contentService.PublishWithStatus(contentService.GetById(id));
        }
    }
}

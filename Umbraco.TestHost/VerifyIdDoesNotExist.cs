﻿using System;
using Umbraco.Core;

namespace Umbraco.TestHost
{
    public class VerifyIdDoesNotExist : MarshalByRefObject
    {
        public void Verify(int id)
        {
            var content = ApplicationContext.Current.Services.ContentService.GetById(id);
            if (content != null)
                throw new Exception("Found content with id " + id);
        }
    }
}

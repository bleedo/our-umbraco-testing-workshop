﻿using System;
using Umbraco.Core;
using Umbraco.Core.Models;

namespace Umbraco.TestHost
{
    public class BlogContextController : MarshalByRefObject
    {
        private IContent blogPost;

        public BlogContext CreateBlogPost()
        {
            var contentService = ApplicationContext.Current.Services.ContentService;
            var parent = contentService.GetById(1075);
            blogPost = contentService.CreateContent("test-post", parent, "BlogPost");
            contentService.SaveAndPublishWithStatus(blogPost, 0, true);
            Console.WriteLine("Created blog post " + blogPost.Name + " (" + blogPost.Id + ")");
            var ctx = new BlogContext
            {
                Id = blogPost.Id,
                Name = blogPost.Name,
                Route = "/" + parent.Name.ToUrlSegment() + "/" + blogPost.Name.ToUrlSegment()
            };
            return ctx;
        }

        public void Cleanup()
        {
            var contentService = ApplicationContext.Current.Services.ContentService;
            contentService.Delete(blogPost);
            Console.WriteLine("Deleted blog post " + blogPost.Name + " (" + blogPost.Id + ")");
        }

        public void RemoveChildren()
        {
            var contentService = ApplicationContext.Current.Services.ContentService;
            var children = contentService.GetChildren(blogPost.Id);
            foreach (var child in children)
                contentService.Delete(child);
        }
    }

    [Serializable]
    public class BlogContext
    {
        public int Id { get; set; }
        public string Route { get; set; }
        public string Name { get; set; }
    }
}

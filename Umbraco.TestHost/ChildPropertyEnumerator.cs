﻿using System;
using System.Collections.Generic;
using Umbraco.Core;

namespace Umbraco.TestHost
{
    public class ChildPropertyEnumerator : MarshalByRefObject
    {
        public List<Dictionary<string, string>> GetChildren(int parentId)
        {
            var list = new List<Dictionary<string, string>>();
            var children = ApplicationContext.Current.Services.ContentService.GetChildren(parentId);
            foreach (var child in children)
            {
                var dict = new Dictionary<string, string>();
                foreach (var prop in child.Properties)
                {
                    dict.Add(prop.Alias, (prop.Value ?? "").ToString());
                }
                list.Add(dict);
            }
            return list;
        }
    }
}

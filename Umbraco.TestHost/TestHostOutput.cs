﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace Umbraco.TestHost
{
    public partial class TestHostOutput : Form
    {
        private static readonly object lockObj = new object();
        public delegate void DisposeDelegate();
        public delegate void AddCharDelegate(char value);

        public TestHostOutput()
        {
            InitializeComponent();
        }

        private void AddCharImpl(char value)
        {
            textBox1.Text += value;
            Application.DoEvents();
        }

        public void AddChar(char value)
        {
            if (InvokeRequired)
                BeginInvoke((AddCharDelegate)AddCharImpl, value);
            else
                AddCharImpl(value);
        }

        public void Kill()
        {
            if (InvokeRequired)
                BeginInvoke((DisposeDelegate) Dispose);
            else
                Dispose();
        }
    }

}

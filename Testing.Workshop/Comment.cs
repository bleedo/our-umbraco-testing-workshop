﻿namespace Testing.Workshop
{
    public class Comment
    {
        public string Text { get; set; }
        public string Email { get; set; }
        public int ParentContentId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Testing.Workshop.Validation;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.PublishedCache;

namespace Testing.Workshop
{
    public class CommentService
    {
        private readonly IContentService contentService;
        private readonly IPublishedContentCache contentCache;
        private readonly int commentContentType;

        public CommentService(IContentService contentService, IPublishedContentCache contentCache, int commentContentType)
        {
            this.contentService = contentService;
            this.contentCache = contentCache;
            this.commentContentType = commentContentType;
        }

        public CommentResult PostComment(Comment comment)
        {
            var createCommentCommand = new CreateCommentCommand(contentService, comment);
            return createCommentCommand.CreateComment();
        }

        public void Approve(IContent comment)
        {
            contentService.Publish(comment, 0);
        }

        public void Deny(IContent comment)
        {
            contentService.Delete(comment, 0);
        }

        public IEnumerable<IContent> GetCommentsToApprove()
        {
            return contentService
                .GetContentOfContentType(commentContentType)
                .Where(c => !c.Published);
        }

        public IEnumerable<IPublishedContent> GetComments(int parentId)
        {
            return contentCache
                .GetById(UmbracoContext.Current, false, parentId)
                .Children("Comment");
        }
    }

    public class CreateCommentCommand
    {
        private readonly IContentService contentService;
        private static readonly EmailValidator EmailValidator = new EmailValidator();
        private readonly CommentResult commentResult = new CommentResult();
        private readonly Comment comment;

        public CreateCommentCommand(IContentService contentService, Comment comment)
        {
            this.contentService = contentService;
            this.comment = comment;
        }

        public CommentResult CreateComment()
        {
            ValidateComment();

            if (IsValid())
                CreateContent();

            return commentResult;
        }

        private void ValidateComment()
        {
            if (InvalidEmail(comment))
                commentResult.Errors.Add("Invalid email address");

            if (InvalidComment(comment))
                commentResult.Errors.Add("Empty comment");
        }

        private bool IsValid()
        {
            return commentResult.Valid;
        }

        private void CreateContent()
        {
            var content = contentService.CreateContent("Comment from " + comment.Email, comment.ParentContentId, "Comment", 0);

            if (content == null)
            {
                commentResult.Errors.Add("Could not create content.");
                return;
            }

            content.SetValue("email", comment.Email);
            content.SetValue("text", comment.Text);

            contentService.Save(content, 0, true);

            commentResult.Id = content.Id;
            commentResult.Success = true;
        }

        private bool InvalidComment(Comment comment)
        {
            return String.IsNullOrWhiteSpace(comment.Text);
        }

        private static bool InvalidEmail(Comment comment)
        {
            return false == EmailValidator.Validate(comment.Email);
        }
    }

    [Serializable]
    public class CommentResult
    {
        public int Id { get; set; }

        public bool Valid => !Errors.Any();

        public bool Success { get; set; }

        public List<string> Errors { get; private set; }

        public CommentResult()
        {
            Errors = new List<string>();
        }
    }
}

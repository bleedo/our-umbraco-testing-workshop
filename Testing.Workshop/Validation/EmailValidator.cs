using System.Text.RegularExpressions;

namespace Testing.Workshop.Validation
{
    public class EmailValidator
    {
        private const string EmailPattern = @"^[^@]{1,64}\@[^@]+\.[^@]+$";
        private static readonly Regex EmailExpression = new Regex(EmailPattern);

        public bool Validate(string emailAddress)
        {
            return emailAddress != null
                && emailAddress.Length <= 128
                && EmailExpression.IsMatch(emailAddress);
        }
    }
}